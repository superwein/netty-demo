package com.example.nettydemo;

import io.lettuce.core.EpollProvider;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class NettyDemoApplicationTests {

    @Test
    void contextLoads() {

        System.out.println(EpollProvider.isAvailable());
    }

}
