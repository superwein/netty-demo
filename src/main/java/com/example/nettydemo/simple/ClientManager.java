package com.example.nettydemo.simple;

import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * 客户端管理
 *
 * @author SuperWein
 */
@Slf4j
public class ClientManager {
    /**
     * 客户端缓存
     */
    public static Map<String, ChannelHandlerContext> Clients =  new HashMap<String, ChannelHandlerContext>();

    public static void addClient(ChannelHandlerContext context) {

    }

    public static ChannelHandlerContext getClient() {
        return null;
    }

    public static void removeClient() {

    }

}
