package com.example.nettydemo.simple;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import lombok.extern.slf4j.Slf4j;

/**
 * Netty Client Channel 处理器
 *
 * @author SuperWein
 */
@Slf4j
public class ChannelClientHandler extends ChannelInboundHandlerAdapter {

    @Override
    public void channelActive(ChannelHandlerContext context) throws Exception {
        log.info("Client connect with server: {} success", context.channel().remoteAddress());
    }

    @Override
    public void channelInactive(ChannelHandlerContext context) throws Exception {
        log.info("Client disConnect with server: {}", context.channel().remoteAddress());
    }

    @Override
    public void channelRead(ChannelHandlerContext context, Object msg) throws Exception {
        log.info("Receive message [{}] from server {}", msg, context.channel().remoteAddress());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext context, Throwable cause) throws Exception {
        log.error("Channel client handler has an exception", cause);
        context.close();
    }

}
